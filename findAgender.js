// 1. Find all people who are Agender

/* {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  } */

function findAgender(data) {
  return data.filter((eachData) => eachData.gender === "Agender");
}

module.exports = findAgender;
