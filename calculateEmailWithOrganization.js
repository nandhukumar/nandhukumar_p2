// 6. Calculate how many .org, .au, .com emails are there

/* {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  } */

function calculateEmailWithOrganization(data) {
  let calculateEmailWithOrg = data.reduce((acc, currentData) => {
    let email = currentData.email;
    if (
      email.endsWith("org") ||
      email.endsWith("au") ||
      email.endsWith("com")
    ) {
      let splittedArray = email.split(".");
      console.log(splittedArray);
      let organization = splittedArray.slice(-1);
      console.log(organization);
      if (organization in acc) {
        acc[organization] += 1;
      } else {
        acc[organization] = 1;
      }
    }
    return acc;
  }, {});

  return calculateEmailWithOrg;
}

module.exports = calculateEmailWithOrganization;
