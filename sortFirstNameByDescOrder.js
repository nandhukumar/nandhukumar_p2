// 7. Sort the data in descending order of first name

/* {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  } */

function sortFirstNameByDescOrder(data) {
  let sortFirstNameByDescendingOrder = data.sort((a, b) =>
    a.first_name < b.first_name ? 1 : -1
  );

  return sortFirstNameByDescendingOrder;
}

module.exports = sortFirstNameByDescOrder;
