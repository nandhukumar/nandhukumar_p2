// 3. Find the sum of all the fourth components of the ip addresses.

/* {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  } */

function findSumOfAllFourthComponentOfIpAddress(data, splitedIpAaddress) {
  let splitIpAddressIntoArray = splitedIpAaddress(data);

  let findSumOfFourthComponenOfArray = splitIpAddressIntoArray.reduce(
    (acc, currentData) => {
      acc += currentData.splitedIpAaddress[3];
      return acc;
    },
    0
  );
  return findSumOfFourthComponenOfArray;
}

module.exports = findSumOfAllFourthComponentOfIpAddress;
