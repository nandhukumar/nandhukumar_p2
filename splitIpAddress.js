// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]

/* {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  } */

function splitIpAddress(data) {
  let splitIpAddressIntoArray = data.map((eachData) => {
    let ipAddress = eachData.ip_address;
    let arrayOfSplitedIpAddress = ipAddress.split(".");

    let convertStringToInteger = arrayOfSplitedIpAddress.map((eachComponent) =>
      parseInt(eachComponent)
    );

    eachData["splitedIpAaddress"] = convertStringToInteger;
    return eachData;
  });
  return splitIpAddressIntoArray;
}

module.exports = splitIpAddress;
