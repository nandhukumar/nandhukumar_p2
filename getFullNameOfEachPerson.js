// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.

/* {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  } */

function getFullNameOfEachPerson(data) {
  let getFullName = data.map((eachData) => {
    let fullName = eachData.first_name + " " + eachData.last_name;

    eachData["full_name"] = fullName;

    return eachData;
  });

  return getFullName;
}

module.exports = getFullNameOfEachPerson;
