// 5. Filter out all the .org emails

/* {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  } */

function findEmailwithDotOrg(data) {
  let filterEmailwithDotOrg = data.filter((eachData) =>
    eachData.email.endsWith(".org")
  );

  return filterEmailwithDotOrg;
}

module.exports = findEmailwithDotOrg;
